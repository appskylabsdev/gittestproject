import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  imgs: string[];

  constructor(public navCtrl: NavController) {
    this.imgs = [
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEwgYPGxyWPPfnRJ9f3YNJRNiqCsqDhOFDTsmo4GKdvs-2nHL4',
      'https://tacosnpolitics.files.wordpress.com/2016/07/ss-call-me.jpg',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIPbOo23J_VzkRxhb4OzNUrlbySDWFfAArkKkunDXtpK_IFlBI',
    ]
  }

}
